// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAuth, GoogleAuthProvider, signInWithPopup } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getFirestore } from "firebase/firestore";

// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDSBmI6YpQgV3xuIKjAPdN4RrM8oQ60fX8",
  authDomain: "clouds-assignment-5-ddaed.firebaseapp.com",
  databaseURL: "https://clouds-assignment-5-ddaed-default-rtdb.firebaseio.com/",
  projectId: "clouds-assignment-5-ddaed",
  storageBucket: "clouds-assignment-5-ddaed.appspot.com",
  messagingSenderId: "886530213852",
  appId: "1:886530213852:web:2304dbfb21677179ed3119",
  measurementId: "G-PF35T4GQMK"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth();

const provider = new GoogleAuthProvider();
provider.setCustomParameters({ prompt: "select_account" });

export const signInWithGoogle = () => signInWithPopup(auth, provider);
export const db = getDatabase(app);
export const dbFirestore = getFirestore(app);
